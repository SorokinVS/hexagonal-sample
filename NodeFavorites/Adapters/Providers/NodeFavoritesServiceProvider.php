<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\Providers;

use Hexagonal\NodeFavorites\Adapters\Out\Persistence\NodeFavoritesRepository;
use Hexagonal\NodeFavorites\Application\Ports\In\AddNodeFavoriteUseCase;
use Hexagonal\NodeFavorites\Application\Ports\In\DeleteNodeFavoriteUseCase;
use Hexagonal\NodeFavorites\Application\Ports\In\EditNodeFavoriteUseCase;
use Hexagonal\NodeFavorites\Application\Ports\In\GetNodeFavoritesUseCase;
use Hexagonal\NodeFavorites\Application\Ports\Out\DeleteNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetEventFlowMapOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetNodeFavoritesOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\SaveNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Application\Services\AddNodeFavoriteService;
use Hexagonal\NodeFavorites\Application\Services\DeleteNodeFavoriteService;
use Hexagonal\NodeFavorites\Application\Services\EditNodeFavoriteService;
use Hexagonal\NodeFavorites\Application\Services\GetNodeFavoritesService;
use Illuminate\Support\ServiceProvider;

class NodeFavoritesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadMigrationsFrom(__DIR__.'/../Data/Migrations');

        /* in ports*/
        $this->app->bind(AddNodeFavoriteUseCase::class, AddNodeFavoriteService::class);
        $this->app->bind(DeleteNodeFavoriteUseCase::class, DeleteNodeFavoriteService::class);
        $this->app->bind(EditNodeFavoriteUseCase::class, EditNodeFavoriteService::class);
        $this->app->bind(GetNodeFavoritesUseCase::class, GetNodeFavoritesService::class);

        /* out ports*/
        $this->app->bind(DeleteNodeFavoriteOutPort::class, NodeFavoritesRepository::class);
        $this->app->bind(GetNodeFavoriteOutPort::class, NodeFavoritesRepository::class);
        $this->app->bind(GetNodeFavoritesOutPort::class, NodeFavoritesRepository::class);
        $this->app->bind(SaveNodeFavoriteOutPort::class, NodeFavoritesRepository::class);
        $this->app->bind(GetEventFlowMapOutPort::class, NodeFavoritesRepository::class);
    }
}
