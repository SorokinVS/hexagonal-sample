<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\In\GraphQL;

use Hexagonal\NodeFavorites\Adapters\Exceptions\RecordNotFoundException;
use Hexagonal\NodeFavorites\Adapters\In\GraphQL\Transformers\NodeFavoritesTransformer;
use Hexagonal\NodeFavorites\Adapters\In\GraphQL\Transformers\NodeFavoriteTransformer;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetEventFlowMapOutPort;
use Hexagonal\NodeFavorites\Domain\NodeFavorite;
use Porto\Containers\Authentication\Exceptions\AuthorizedUserNotFoundException;
use Porto\Containers\Authentication\Traits\AuthenticatedUserTrait;
use Porto\Containers\User\Models\User;

abstract class BaseGraphqlQuery
{
    use AuthenticatedUserTrait;

    /**
     * @var User
     */
    private User $currentUser;

    /**
     * @var GetEventFlowMapOutPort
     */
    private GetEventFlowMapOutPort $repository;

    /**
     * BaseGraphqlQuery constructor.
     * @param GetEventFlowMapOutPort $repository
     */
    public function __construct(GetEventFlowMapOutPort $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param NodeFavorite $data
     * @return array
     * @throws RecordNotFoundException
     */
    protected function transformOneResult(NodeFavorite $data):array
    {
        $transformer = new NodeFavoriteTransformer();
        $mapOfEventFlowNode = $this->repository->getMapOfEventFlowNode();
        if (empty($mapOfEventFlowNode[$data->getNodeId()])) {
            throw new RecordNotFoundException();
        }

        return $transformer->setEventFlowNode($mapOfEventFlowNode[$data->getNodeId()])
            ->setNodeFavorite($data)->transform();
    }

    /**
     * @param array $data
     * @return array
     */
    protected function transformMultipleResult(array $data):array
    {
        $transformer = new NodeFavoritesTransformer();
        $mapOfEventFlowNode = $this->repository->getMapOfEventFlowNode();

        return $transformer->setMapOfEventFlowNode($mapOfEventFlowNode)->setNodeFavorites($data)->transform();
    }

    /**
     * @return User
     * @throws AuthorizedUserNotFoundException
     */
    protected function getUser(): User
    {
        if (!empty($this->currentUser)) {
            return $this->currentUser;
        }
        return $this->currentUser = $this->getAuthenticatedUser();
    }
}
