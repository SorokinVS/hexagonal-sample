<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\In\GraphQL\Mutations;

use Hexagonal\NodeFavorites\Adapters\In\GraphQL\BaseGraphqlQuery;
use Hexagonal\NodeFavorites\Application\Ports\In\DeleteNodeFavoriteUseCase;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetEventFlowMapOutPort;
use Porto\Containers\Authentication\Exceptions\AuthorizedUserNotFoundException;

/**
 * Class RemoveNodeFavoriteMutation.
 */
class RemoveNodeFavoriteMutation extends BaseGraphqlQuery
{
    private DeleteNodeFavoriteUseCase $deleteNodeFavoriteUseCase;

    /**
     * RemoveNodeFavoriteMutation constructor.
     * @param DeleteNodeFavoriteUseCase $deleteNodeFavoriteUseCase
     * @param GetEventFlowMapOutPort $repository
     */
    public function __construct(
        DeleteNodeFavoriteUseCase $deleteNodeFavoriteUseCase,
        GetEventFlowMapOutPort $repository
    ) {
        parent::__construct($repository);
        $this->deleteNodeFavoriteUseCase = $deleteNodeFavoriteUseCase;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @return bool
     * @throws AuthorizedUserNotFoundException
     */
    public function __invoke($rootValue, array $args): bool
    {
        $this->deleteNodeFavoriteUseCase->deleteNodeFavorite($args['nodeActionId'], $this->getUser()->id);

        return true;
    }
}
