<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\In\GraphQL\Mutations;

use Hexagonal\NodeFavorites\Adapters\Exceptions\RecordNotFoundException;
use Hexagonal\NodeFavorites\Adapters\In\GraphQL\BaseGraphqlQuery;
use Hexagonal\NodeFavorites\Application\Ports\In\AddNodeFavoriteUseCase;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetEventFlowMapOutPort;
use Porto\Containers\Authentication\Exceptions\AuthorizedUserNotFoundException;

/**
 * Class AddNodeFavoriteMutation.
 */
class AddNodeFavoriteMutation extends BaseGraphqlQuery
{
    private AddNodeFavoriteUseCase $addNodeFavoriteUseCase;

    /**
     * AddNodeFavoriteMutation constructor.
     * @param AddNodeFavoriteUseCase $addNodeFavoriteUseCase
     * @param GetEventFlowMapOutPort $repository
     */
    public function __construct(AddNodeFavoriteUseCase $addNodeFavoriteUseCase, GetEventFlowMapOutPort $repository)
    {
        parent::__construct($repository);
        $this->addNodeFavoriteUseCase = $addNodeFavoriteUseCase;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @return array
     * @throws AuthorizedUserNotFoundException
     * @throws RecordNotFoundException
     */
    public function __invoke($rootValue, array $args): array
    {
        $data = $this->addNodeFavoriteUseCase->addNodeFavorite(
            $args['favorite']['nodeActionId'],
            $this->getUser()->id,
            $args['favorite']['iconName'] ?? null
        );

        return $this->transformOneResult($data);
    }
}
