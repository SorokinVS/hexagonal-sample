<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\In\GraphQL\Mutations;

use Hexagonal\NodeFavorites\Adapters\Exceptions\RecordNotFoundException;
use Hexagonal\NodeFavorites\Adapters\In\GraphQL\BaseGraphqlQuery;
use Hexagonal\NodeFavorites\Application\Ports\In\EditNodeFavoriteUseCase;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetEventFlowMapOutPort;
use Porto\Containers\Authentication\Exceptions\AuthorizedUserNotFoundException;

/**
 * Class EditNodeFavoriteMutation.
 */
class EditNodeFavoriteMutation extends BaseGraphqlQuery
{
    /**
     * @var EditNodeFavoriteUseCase
     */
    private EditNodeFavoriteUseCase $editNodeFavoriteUseCase;

    /**
     * EditNodeFavoriteMutation constructor.
     * @param EditNodeFavoriteUseCase $editNodeFavoriteUseCase
     * @param GetEventFlowMapOutPort $repository
     */
    public function __construct(EditNodeFavoriteUseCase $editNodeFavoriteUseCase, GetEventFlowMapOutPort $repository)
    {
        parent::__construct($repository);
        $this->editNodeFavoriteUseCase = $editNodeFavoriteUseCase;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @return array
     * @throws AuthorizedUserNotFoundException
     * @throws RecordNotFoundException
     */
    public function __invoke($rootValue, array $args): array
    {
        $data = $this->editNodeFavoriteUseCase->editNodeFavorite(
            $args['favorite']['nodeActionId'],
            $this->getUser()->id,
            $args['favorite']['iconName'] ?? null
        );

        return $this->transformOneResult($data);
    }
}
