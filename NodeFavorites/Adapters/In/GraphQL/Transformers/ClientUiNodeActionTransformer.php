<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\In\GraphQL\Transformers;

use Porto\Ship\Parents\Contracts\TransformerContract;

class ClientUiNodeActionTransformer implements TransformerContract
{
    private array $node;

    public function transform(): array
    {
        $params = $this->node->params ?? [];

        return [
            'id'              => $this->node['id'],
            'type'            => $this->node['type'],
            'title'           => $this->node['title'],
            'filterUserInput' => $this->getUserInputs($params['filtration'] ?? []),
            'valuesUserInput' => $this->getUserInputs($params['attributes'] ?? [], $this->node['entity_id'])
        ];
    }

    /**
     * @param array $node
     * @return self
     */
    public function setNode(array $node): self
    {
        $this->node = $node;

        return $this;
    }

    /**
     * @param array    $params
     * @param int|null $entityId
     *
     * @return array
     */
    private function getUserInputs(array $params = [], int $entityId = null): array
    {
        $userInputs = [];

        foreach ($params as $key => $value) {
            if ($value === 'user_input') {
                $item = [
                    'attributeId' => $params['attributeId'],
                    'entityId'    => $params['entityId'] ?? $entityId
                ];

                $userInputs[] = $item;
            }
            if (is_array($value)) {
                $userInputs = array_merge($this->getUserInputs($value, $entityId), $userInputs);
            }
        }

        return array_reverse($userInputs);
    }
}
