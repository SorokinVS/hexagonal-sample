<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\In\GraphQL\Transformers;

use Hexagonal\NodeFavorites\Domain\NodeFavorite;
use Porto\Ship\Parents\Contracts\TransformerContract;

/**
 * Class NodeFavoritesTransformer.
 */
class NodeFavoritesTransformer implements TransformerContract
{
    /**
     * @var NodeFavorite[]
     */
    private array $nodeFavorites;

    /**
     * @var array[]
     */
    private array $mapOfEventFlowNode;

    /**
     * @return array
     */
    public function transform(): array
    {
        $nodeFavoriteTransformer = new NodeFavoriteTransformer();

        $result = [];
        foreach ($this->nodeFavorites as $favorite) {
            if (empty($this->mapOfEventFlowNode[$favorite->getNodeId()])) {
                continue;
            }
            $nodeFavoriteTransformer->setEventFlowNode($this->mapOfEventFlowNode[$favorite->getNodeId()]);
            $nodeFavoriteTransformer->setNodeFavorite($favorite);
            $result[] = $nodeFavoriteTransformer->transform();
        }

        return $result;
    }

    /**
     * @param NodeFavorite[] $nodeFavorites
     * @return self
     */
    public function setNodeFavorites(array $nodeFavorites): self
    {
        $this->nodeFavorites = $nodeFavorites;

        return $this;
    }

    /**
     * @param array[] $mapOfEventFlowNode
     * @return self
     */
    public function setMapOfEventFlowNode(array $mapOfEventFlowNode): self
    {
        $this->mapOfEventFlowNode = $mapOfEventFlowNode;

        return $this;
    }
}
