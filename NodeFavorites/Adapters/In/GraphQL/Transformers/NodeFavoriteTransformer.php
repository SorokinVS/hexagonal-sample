<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\In\GraphQL\Transformers;

use Hexagonal\NodeFavorites\Domain\NodeFavorite;
use Porto\Ship\Parents\Contracts\TransformerContract;

/**
 * Class NodeFavoriteTransformer.
 */
class NodeFavoriteTransformer implements TransformerContract
{
    /**
     * @var NodeFavorite
     */
    private NodeFavorite $nodeFavorite;

    /**
     * @var array
     */
    private array $eventFlowNode;

    /**
     * @return array
     */
    public function transform(): array
    {
        $nodeTransformer = new ClientUiNodeActionTransformer();
        $nodeTransformer->setNode($this->eventFlowNode);

        return [
            'nodeAction' => $nodeTransformer->transform(),
            'iconName' => $this->nodeFavorite->getIconName(),
        ];
    }

    /**
     * @param NodeFavorite $nodeFavorite
     * @return self
     */
    public function setNodeFavorite(NodeFavorite $nodeFavorite): self
    {
        $this->nodeFavorite = $nodeFavorite;

        return $this;
    }

    /**
     * @param array[] $mapOfEventFlowNode
     * @return self
     */
    public function setEventFlowNode(array $mapOfEventFlowNode): self
    {
        $this->eventFlowNode = $mapOfEventFlowNode;

        return $this;
    }
}
