<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\In\GraphQL\Queries;

use Hexagonal\NodeFavorites\Adapters\In\GraphQL\BaseGraphqlQuery;
use Hexagonal\NodeFavorites\Application\Ports\In\GetNodeFavoritesUseCase;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetEventFlowMapOutPort;
use Porto\Containers\Authentication\Exceptions\AuthorizedUserNotFoundException;

/**
 * Class GetFavoritesQuery.
 */
class GetNodeFavoritesQuery extends BaseGraphqlQuery
{
    private GetNodeFavoritesUseCase $getNodeFavoritesUseCase;

    /**
     * GetNodeFavoritesQuery constructor.
     * @param GetNodeFavoritesUseCase $getNodeFavoritesUseCase
     * @param GetEventFlowMapOutPort $repository
     * @throws AuthorizedUserNotFoundException
     */
    public function __construct(GetNodeFavoritesUseCase $getNodeFavoritesUseCase, GetEventFlowMapOutPort $repository)
    {
        parent::__construct($repository);
        $this->getNodeFavoritesUseCase = $getNodeFavoritesUseCase;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @return array
     * @throws AuthorizedUserNotFoundException
     */
    public function __invoke($rootValue, array $args): array
    {
        return $this->transformMultipleResult(
            $this->getNodeFavoritesUseCase->getNodeFavorites($this->getUser()->id)
        );
    }
}
