<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\Exceptions;

use Exception as ExceptionAlias;

/**
 * Class NotFoundException.
 */
class RecordNotFoundException extends ExceptionAlias
{
}
