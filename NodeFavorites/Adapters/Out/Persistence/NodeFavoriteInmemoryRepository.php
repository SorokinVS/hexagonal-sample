<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\Out\Persistence;

use DateTime;
use Hexagonal\NodeFavorites\Adapters\Exceptions\RecordNotFoundException;
use Hexagonal\NodeFavorites\Application\Ports\Out\DeleteNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetEventFlowMapOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetNodeFavoritesOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\SaveNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Domain\NodeFavorite;
use Porto\Containers\User\Models\User;

/**
 * Class NodeFavoriteInmemoryRepository.
 */
class NodeFavoriteInmemoryRepository implements
    SaveNodeFavoriteOutPort,
    DeleteNodeFavoriteOutPort,
    GetNodeFavoriteOutPort,
    GetNodeFavoritesOutPort,
    GetEventFlowMapOutPort
{
    /** @var NodeFavorite[] */
    private array $data = [];

    /**
     * @param int $userId
     * @param int $eventflowNodeId
     * @return mixed|void
     */
    public function deleteNodeFavorite(int $userId, int $eventflowNodeId)
    {
        foreach ($this->data as $key => $item) {
            if ($item->getUserId() === $userId && $item->getNodeId() === $eventflowNodeId) {
                unset($this->data[$key]);
            }
        }
    }

    /**
     * @param int $userId
     * @param int $eventflowNodeId
     * @return NodeFavorite
     * @throws RecordNotFoundException
     */
    public function getNodeFavorite(int $userId, int $eventflowNodeId): NodeFavorite
    {
        foreach ($this->data as $item) {
            if ($item->getUserId() === $userId &&
                $item->getNodeId() === $eventflowNodeId) {
                return $item;
            }
        }

        throw new RecordNotFoundException();
    }

    /**
     * @param int $userId
     * @return NodeFavorite[]
     */
    public function getNodeFavorites(int $userId): array
    {
        $result = [];
        foreach ($this->data as $item) {
            if ($item->getUserId() === $userId) {
                $result[] = $item;
            }
        }

        return $result;
    }

    public function saveNodeFavorite(NodeFavorite $favorite): NodeFavorite
    {
        if (empty($favorite->getId())) {
            $favorite->setId($this->getNextNewId());
        }

        $this->data[$favorite->getId()] = $favorite;

        return $favorite;
    }

    private function getNextNewId()
    {
        $maxId = 0;
        foreach ($this->data as $item) {
            if ($item->getId() > $maxId) {
                $maxId = $item->getId();
            }
        }

        return $maxId + 1;
    }

    /**
     * @param User|null $user
     * @return array[]
     */
    public function getMapOfEventFlowNode(User $user = null):array
    {
        $result = [];
        for ($i = 1; $i <= 10; $i++) {
            $node = [];
            $node['id'] = $i;
            $node['eventflow_id'] = $i;
            $node['type'] = 'select';
            $node['title'] = 'title_'.$i;
            $node['params'] = [];
            $node['lat'] = 1;
            $node['lng'] = 2;
            $node['created_at'] = new DateTime();
            $node['updated_at'] = new DateTime();
            $node['deleted_at'] = null;
            $node['description'] = 'STUB';
            $node['entity_id'] = rand(0, $i);
            $node['columns'] = [];

            $result[$i] = $node;
        }

        return $result;
    }
}
