<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Adapters\Out\Persistence;

use DateTime;
use DB;
use Exception;
use Hexagonal\NodeFavorites\Adapters\Exceptions\RecordNotFoundException;
use Hexagonal\NodeFavorites\Application\Ports\Out\DeleteNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetEventFlowMapOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetNodeFavoritesOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\SaveNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Domain\NodeFavorite;
use Illuminate\Support\Facades\Request;

/**
 * Class NodeFavoritesRepository.
 */
class NodeFavoritesRepository implements
    SaveNodeFavoriteOutPort,
    DeleteNodeFavoriteOutPort,
    GetNodeFavoriteOutPort,
    GetNodeFavoritesOutPort,
    GetEventFlowMapOutPort
{
    private int $applicationId;

    public function __construct()
    {
        $this->applicationId = (int) Request::header('Application', 0);
    }

    /**
     * @param int $userId
     * @param int $eventflowNodeId
     * @return mixed|void
     */
    public function deleteNodeFavorite(int $userId, int $eventflowNodeId)
    {
        DB::table('eventflow_favorite_nodes')
            ->where('user_id', '=', $userId)
            ->where('eventflow_node_id', '=', $eventflowNodeId)
            ->update(['deleted_at' => new DateTime()]);
    }

    /**
     * @param $id
     * @return NodeFavorite
     * @throws RecordNotFoundException
     */
    public function getFavoriteById($id)
    {
        $favorite = DB::table('eventflow_favorite_nodes')
            ->select()
            ->where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();

        if (empty($favorite)) {
            throw new RecordNotFoundException();
        }
        $favorite = $this->stdClassToArray($favorite);

        return $this->getModelFromDbResult($favorite);
    }

    /**
     * @param int $userId
     * @param int $eventflowNodeId
     * @return NodeFavorite
     * @throws RecordNotFoundException
     */
    public function getNodeFavorite(int $userId, int $eventflowNodeId): NodeFavorite
    {
        $favorite = DB::table('eventflow_favorite_nodes')
            ->select()
            ->where('user_id', '=', $userId)
            ->where('eventflow_node_id', '=', $eventflowNodeId)
            ->whereNull('deleted_at')
            ->first();

        if (empty($favorite)) {
            throw new RecordNotFoundException();
        }

        $favorite = $this->stdClassToArray($favorite);

        return $this->getModelFromDbResult($favorite);
    }

    /**
     * @param int $userId
     * @return NodeFavorite[]
     */
    public function getNodeFavorites(int $userId): array
    {
        $favorites = DB::table('eventflow_favorite_nodes')
            ->select()
            ->where('user_id', '=', $userId)
            ->whereNull('deleted_at')
            ->get()->toArray();

        $favorites = $this->stdClassToArray($favorites);

        $result = [];
        foreach ($favorites as $favorite) {
            $result[] = $this->getModelFromDbResult($favorite);
        }

        return $result;
    }

    /**
     * @param NodeFavorite $favorite
     * @return NodeFavorite
     * @throws RecordNotFoundException
     * @throws Exception
     */
    public function saveNodeFavorite(NodeFavorite $favorite): NodeFavorite
    {
        $date = new DateTime();
        if ($favorite->getId()) {
            DB::table('eventflow_favorite_nodes')
                ->where('id', '=', $favorite->getId())
                ->update(['icon_name' => $favorite->getIconName(), 'updated_at' => $date]);
        } else {
            $map = $this->getMapOfEventFlowNode();
            if (empty($map[$favorite->getNodeId()])) {
                throw new RecordNotFoundException();
            }
            DB::table('eventflow_favorite_nodes')->insert(
                [
                    'eventflow_node_id' => $favorite->getNodeId(),
                    'user_id' => $favorite->getUserId(),
                    'icon_name' => $favorite->getIconName(),
                    'created_at' => $date,
                    'updated_at' => $date,
                ]
            );

            $favoriteResult = $this->getNodeFavorite($favorite->getUserId(), $favorite->getNodeId());
            $favorite->setId($favoriteResult->getId());
        }

        return $favorite;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getMapOfEventFlowNode():array
    {
        $nodes = $this->stdClassToArray(
            DB::table('eventflow_nodes')
                ->select(['eventflow_nodes.*'])
                ->join('eventflows', 'eventflow_nodes.eventflow_id', '=', 'eventflows.id')
                ->whereNull('eventflow_nodes.deleted_at')
                ->where('eventflows.application_id', '=', $this->applicationId)
                ->get()->toArray()
        );

        $result = [];
        foreach ($nodes as $item) {
            $item['params'] = json_decode($item['params'], true);
            $item['columns'] = json_decode($item['columns'], true);
            $item['created_at'] = new DateTime($item['created_at']);
            $item['updated_at'] = new DateTime($item['updated_at']);
            $result[$item['id']] = $item;
        }

        return $result;
    }

    private function stdClassToArray($result)
    {
        return json_decode(json_encode($result), true);
    }

    /**
     * @param $result
     * @return NodeFavorite
     */
    private function getModelFromDbResult($result):NodeFavorite
    {
        $favorite = new NodeFavorite($result['eventflow_node_id'], $result['user_id']);
        $favorite
            ->setId($result['id'] ?? null)
            ->setIconName($result['icon_name']);

        return $favorite;
    }
}
