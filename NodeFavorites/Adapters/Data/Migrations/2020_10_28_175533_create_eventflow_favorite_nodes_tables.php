<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateEventflowFavoriteNodesTables.
 */
class CreateEventflowFavoriteNodesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventflow_favorite_nodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('eventflow_node_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->string('icon_name')->nullable(true);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('eventflow_node_id')->references('id')->on('eventflow_nodes');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventflow_favorite_nodes');
    }
}
