<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Domain;

/**
 * Class NodeFavorite.
 */
class NodeFavorite
{
    /**
     * @var int|null
     */
    private ?int $id;

    /**
     * @var int
     */
    private int $nodeId;

    /**
     * @var int
     */
    private int $userId;

    /**
     * @var string|null
     */
    private ?string $iconName;

    /**
     * NodeFavorite constructor.
     * @param int $nodeId
     * @param int $userId
     */
    public function __construct(int $nodeId, int $userId)
    {
        $this->nodeId = $nodeId;
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getNodeId(): int
    {
        return $this->nodeId;
    }

    /**
     * @param int $nodeId
     * @return self
     */
    public function setNodeId(int $nodeId): self
    {
        $this->nodeId = $nodeId;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return self
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIconName(): ?string
    {
        return $this->iconName;
    }

    /**
     * @param string|null $iconName
     * @return self
     */
    public function setIconName(?string $iconName): self
    {
        $this->iconName = $iconName;

        return $this;
    }
}
