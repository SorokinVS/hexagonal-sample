<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Services;

use Hexagonal\NodeFavorites\Application\Ports\In\GetNodeFavoritesUseCase;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetNodeFavoritesOutPort;

/**
 * Class GetNodeFavoritesService.
 */
class GetNodeFavoritesService implements GetNodeFavoritesUseCase
{
    private GetNodeFavoritesOutPort $repository;

    /**
     * GetNodeFavoritesService constructor.
     * @param GetNodeFavoritesOutPort $repository
     */
    public function __construct(GetNodeFavoritesOutPort $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getNodeFavorites(int $userId): array
    {
        return $this->repository->getNodeFavorites($userId);
    }
}
