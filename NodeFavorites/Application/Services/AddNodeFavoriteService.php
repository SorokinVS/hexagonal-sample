<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Services;

use Exception;
use Hexagonal\NodeFavorites\Application\Ports\In\AddNodeFavoriteUseCase;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\SaveNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Domain\NodeFavorite;

/**
 * Class AddNodeFavoriteService.
 */
class AddNodeFavoriteService implements AddNodeFavoriteUseCase
{
    private SaveNodeFavoriteOutPort $saveRepository;
    private GetNodeFavoriteOutPort $findRepository;

    /**
     * AddNodeFavoriteService constructor.
     * @param SaveNodeFavoriteOutPort $repository
     * @param GetNodeFavoriteOutPort $findRepository
     */
    public function __construct(SaveNodeFavoriteOutPort $repository, GetNodeFavoriteOutPort $findRepository)
    {
        $this->saveRepository = $repository;
        $this->findRepository = $findRepository;
    }

    /**
     * @param int $eventflowNodeId
     * @param int $userId
     * @param string|null $iconName
     * @return NodeFavorite
     */
    public function addNodeFavorite(int $eventflowNodeId, int $userId, ?string $iconName): NodeFavorite
    {
        $favorite = $this->getNodeFavorite($eventflowNodeId, $userId);
        $favorite
            ->setNodeId($eventflowNodeId)
            ->setUserId($userId)
            ->setIconName($iconName);

        return $this->saveRepository->saveNodeFavorite($favorite);
    }

    /**
     * @param int $eventflowNodeId
     * @param int $userId
     * @return NodeFavorite
     */
    private function getNodeFavorite(int $eventflowNodeId, int $userId): NodeFavorite
    {
        try {
            $favorite = $this->findRepository->getNodeFavorite($userId, $eventflowNodeId);
        } catch (Exception $e) {
            $favorite = new NodeFavorite($eventflowNodeId, $userId);
            $favorite->setId(null);
        }

        return $favorite;
    }
}
