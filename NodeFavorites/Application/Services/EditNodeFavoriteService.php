<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Services;

use Hexagonal\NodeFavorites\Application\Ports\In\EditNodeFavoriteUseCase;
use Hexagonal\NodeFavorites\Application\Ports\Out\GetNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Application\Ports\Out\SaveNodeFavoriteOutPort;
use Hexagonal\NodeFavorites\Domain\NodeFavorite;

/**
 * Class EditNodeFavoriteService.
 */
class EditNodeFavoriteService implements EditNodeFavoriteUseCase
{
    private GetNodeFavoriteOutPort $getFavoriteRepository;
    private SaveNodeFavoriteOutPort $saveFavoriteRepository;

    /**
     * EditNodeFavoriteService constructor.
     * @param GetNodeFavoriteOutPort $getFavoriteRepository
     * @param SaveNodeFavoriteOutPort $saveFavoriteRepository
     */
    public function __construct(
        GetNodeFavoriteOutPort $getFavoriteRepository,
        SaveNodeFavoriteOutPort $saveFavoriteRepository
    ) {
        $this->getFavoriteRepository = $getFavoriteRepository;
        $this->saveFavoriteRepository = $saveFavoriteRepository;
    }

    /**
     * @param int $eventflowNodeId
     * @param int $userId
     * @param string|null $iconName
     * @return NodeFavorite
     */
    public function editNodeFavorite(int $eventflowNodeId, int $userId, ?string $iconName): NodeFavorite
    {
        $favorite = $this->getFavoriteRepository->getNodeFavorite($userId, $eventflowNodeId);
        $favorite->setIconName($iconName);

        return $this->saveFavoriteRepository->saveNodeFavorite($favorite);
    }
}
