<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Services;

use Hexagonal\NodeFavorites\Application\Ports\In\DeleteNodeFavoriteUseCase;
use Hexagonal\NodeFavorites\Application\Ports\Out\DeleteNodeFavoriteOutPort;

/**
 * Class DeleteNodeFavoriteService.
 */
class DeleteNodeFavoriteService implements DeleteNodeFavoriteUseCase
{
    private DeleteNodeFavoriteOutPort $deleteFavoriteRepository;

    /**
     * DeleteNodeFavoriteService constructor.
     * @param DeleteNodeFavoriteOutPort $deleteFavoriteRepository
     */
    public function __construct(
        DeleteNodeFavoriteOutPort $deleteFavoriteRepository
    ) {
        $this->deleteFavoriteRepository = $deleteFavoriteRepository;
    }

    /**
     * @param int $eventflowNodeId
     * @param int $userId
     * @return bool
     */
    public function deleteNodeFavorite(int $eventflowNodeId, int $userId): bool
    {
        $this->deleteFavoriteRepository->deleteNodeFavorite($userId, $eventflowNodeId);

        return true;
    }
}
