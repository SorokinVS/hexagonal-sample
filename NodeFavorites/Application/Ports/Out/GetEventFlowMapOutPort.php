<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Ports\Out;

interface GetEventFlowMapOutPort
{
    public function getMapOfEventFlowNode():array;
}
