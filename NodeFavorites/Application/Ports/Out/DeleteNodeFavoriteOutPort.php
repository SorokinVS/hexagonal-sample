<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Ports\Out;

/**
 * Interface DeleteFavoritePort.
 */
interface DeleteNodeFavoriteOutPort
{
    /**
     * @param int $userId
     * @param int $eventflowNodeId
     * @return mixed
     */
    public function deleteNodeFavorite(int $userId, int $eventflowNodeId);
}
