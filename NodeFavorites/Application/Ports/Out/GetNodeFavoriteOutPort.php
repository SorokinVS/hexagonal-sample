<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Ports\Out;

use Hexagonal\NodeFavorites\Domain\NodeFavorite;

/**
 * Interface GetFavoritePort.
 */
interface GetNodeFavoriteOutPort
{
    /**
     * @param int $userId
     * @param int $eventflowNodeId
     * @return NodeFavorite
     */
    public function getNodeFavorite(int $userId, int $eventflowNodeId):NodeFavorite;
}
