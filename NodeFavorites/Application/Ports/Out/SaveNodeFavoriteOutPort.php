<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Ports\Out;

use Hexagonal\NodeFavorites\Domain\NodeFavorite;

/**
 * Interface SaveFavoritePort.
 */
interface SaveNodeFavoriteOutPort
{
    /**
     * @param NodeFavorite $favorite
     * @return NodeFavorite
     */
    public function saveNodeFavorite(NodeFavorite $favorite):NodeFavorite;
}
