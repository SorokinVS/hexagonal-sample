<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Ports\Out;

use Hexagonal\NodeFavorites\Domain\NodeFavorite;

/**
 * Interface GetFavoritesPort.
 */
interface GetNodeFavoritesOutPort
{
    /**
     * @param int $userId
     * @return NodeFavorite[]
     */
    public function getNodeFavorites(int $userId):array;
}
