<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Ports\In;

use Hexagonal\NodeFavorites\Domain\NodeFavorite;

/**
 * Interface AddNodeFavoriteUseCase.
 */
interface AddNodeFavoriteUseCase
{
    /**
     * @param int $eventflowNodeId
     * @param int $userId
     * @param string|null $iconName
     * @return NodeFavorite
     */
    public function addNodeFavorite(int $eventflowNodeId, int $userId, ?string $iconName): NodeFavorite;
}
