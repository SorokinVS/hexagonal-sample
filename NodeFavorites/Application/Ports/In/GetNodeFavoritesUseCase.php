<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Ports\In;

use Hexagonal\NodeFavorites\Domain\NodeFavorite;

/**
 * Interface GetNodeFavoritesUseCase.
 */
interface GetNodeFavoritesUseCase
{
    /**
     * @param int $userId
     * @return NodeFavorite[]
     */
    public function getNodeFavorites(int $userId):array;
}
