<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Application\Ports\In;

/**
 * Interface DeleteNodeFavoriteUseCase.
 */
interface DeleteNodeFavoriteUseCase
{
    /**
     * @param int $eventflowNodeId
     * @param int $userId
     * @return bool
     */
    public function deleteNodeFavorite(int $eventflowNodeId, int $userId): bool;
}
