<?php

declare(strict_types=1);

namespace Hexagonal\NodeFavorites\Tests\Unit;

use Hexagonal\NodeFavorites\Adapters\Out\Persistence\NodeFavoriteInmemoryRepository;
use Hexagonal\NodeFavorites\Application\Services\AddNodeFavoriteService;
use Hexagonal\NodeFavorites\Application\Services\DeleteNodeFavoriteService;
use Hexagonal\NodeFavorites\Application\Services\EditNodeFavoriteService;
use Hexagonal\NodeFavorites\Application\Services\GetNodeFavoritesService;
use Hexagonal\NodeFavorites\Domain\NodeFavorite;
use PHPUnit\Framework\TestCase;

class NodeFavoriteTest extends TestCase
{
    private const USER_ID = 777;

    private NodeFavoriteInmemoryRepository $repository;

    protected function setUp(): void
    {
        $this->repository = new NodeFavoriteInmemoryRepository();
    }

    public function testNodeFavoriteModelApplication(): void
    {
        $object = new NodeFavorite(2, 3);
        $object->setId(1)->setIconName('ico');
        $this->assertEquals(1, $object->getId());
        $this->assertEquals(2, $object->getNodeId());
        $this->assertEquals(3, $object->getUserId());
        $this->assertEquals('ico', $object->getIconName());
    }

    public function testAddNodeFavoriteService(): void
    {
        $nodeFavorite = $this->addNodeFavorite(1, self::USER_ID, 'ico1');

        $this->assertNotEmpty($nodeFavorite->getId());
        $this->assertEquals(1, $nodeFavorite->getNodeId());
        $this->assertEquals(self::USER_ID, $nodeFavorite->getUserId());
        $this->assertEquals('ico1', $nodeFavorite->getIconName());
    }

    public function testGetNodeFavoritesService(): void
    {
        $this->addNodeFavorite(1, self::USER_ID, 'ico1');

        $getService = new GetNodeFavoritesService($this->repository);
        $this->assertNotEmpty($getService->getNodeFavorites(self::USER_ID));
        $this->assertEmpty($getService->getNodeFavorites(self::USER_ID + 1));
    }

    public function testEditNodeFavoritesService(): void
    {
        $nodeFavorite = $this->addNodeFavorite(1, self::USER_ID, 'ico1');
        $id = $nodeFavorite->getId();

        $editService = new EditNodeFavoriteService($this->repository, $this->repository);
        $editService->editNodeFavorite(1, self::USER_ID, 'ico2');

        $this->assertNotEmpty($nodeFavorite->getId());
        $this->assertEquals(1, $nodeFavorite->getNodeId());
        $this->assertEquals($id, $nodeFavorite->getId());
        $this->assertEquals(self::USER_ID, $nodeFavorite->getUserId());
        $this->assertEquals('ico2', $nodeFavorite->getIconName());
    }

    public function testDeleteNodeFavoritesService(): void
    {
        $this->addNodeFavorite(1, self::USER_ID, 'ico1');
        $getService = new GetNodeFavoritesService($this->repository);
        $deleteService = new DeleteNodeFavoriteService($this->repository);
        $this->assertNotEmpty($getService->getNodeFavorites(self::USER_ID));
        $deleteService->deleteNodeFavorite(1, self::USER_ID);
        $this->assertEmpty($getService->getNodeFavorites(self::USER_ID));
    }

    private function addNodeFavorite($nodeId, $userId, $icoName): NodeFavorite
    {
        $addService = new AddNodeFavoriteService($this->repository, $this->repository);

        return $addService->addNodeFavorite($nodeId, $userId, $icoName);
    }
}
